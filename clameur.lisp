;; Clameur.lisp -- Rust Code Creation Library

;; License: GPLv3

;; This file is part of Clameur

;; Clameur is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; Clameur is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Clameur. If not, see <https://www.gnu.org/licenses/>.

;; Author: Will Sinatra <wpsinatra@gmail.com>

;;;; Code:
; Will package later
;(defpackage #:clameur
;  (:use :cl)
;  (:export :l->r
;	   :r/fn
;	   :r/cfn
;	   :r/let
;	   :r/if
;	   :r/elif
;	   :r/else
;	   :r/for
;	   :r/ret
;	   :r/expr
;	   :r/println!
;	   :r/}
;	   :r/nl))

;(in-package clameur)

;; ===== Lisp to Rust Conversion =======
(defmacro r/fn (name args &rest body)
  "Macro that writes a rust style fn x () { line"
  `(concatenate 'string
		"fn " ,name ,args " { " '(#\Newline) ,@body '(#\Newline) "}"))

(defmacro r/cfn (name args &optional body)
  "Create custom Rust function"
  `(concatenate 'string ,name ,args "{" '(#\Newline) ,@body "}"))

(defmacro r/let (args lex)
  "Create a Rust lexical bind"
  `(concatenate 'string "let " ,args " = " ,lex ";" '(#\Newline)))

(defmacro r/let{ (args lex &rest body)
  `(concatenate 'string "let " ,args " = " ,lex " {" '(#\Newline) ,@body '(#\Newline) "};"))

(defmacro w/input (input &optional iargs)
  `(concatenate 'string
		"." ,input "(" ,iargs ")"))
  
(defmacro w/expect (&optional eargs)
  `(concatenate 'string
		".expect(" ,eargs ");"))

(defmacro w/ercs (ok okc err errc)
  `(concatenate 'string "Ok(" ,ok ")" " => " ,okc "," '(#\Newline) "Err(" ,err ")" " => " ,errc '(#\Newline)))

(defmacro w/mat (cval mval)
  `(concatenate 'string
		,cval " => " ,mval "," '(#\Newline)))

(defmacro r/io (input iargs expect)
  `(concatenate 'string "io::" ,input ,iargs '(#\Newline) ,expect))

(defmacro r/match (name &rest body)
  `(concatenate 'string
		"match " ,name " {" '(#\Newline) ,@body "}" '(#\Newline)))
	
(defmacro r/if (condname condargs condact &optional &rest body)
  "Create Rust If struct"
  `(concatenate 'string "if " ,condname,condargs " {" '(#\Newline) ,condact ,@body))

(defmacro r/elif (condname condargs condact)
  "Create Rust Else If Statement"
  `(concatenate 'string "} else if " ,condname,condargs " {" '(#\Newline) ,condact))

(defmacro r/else (condact)
  "Create Rust Else Statement"
  `(concatenate 'string "} else {" '(#\Newline) ,condact "}"))

(defmacro r/for (forx conditional action)
  "Create Rust For Statement"
  `(concatenate 'string "for " ,forx " in " ,conditional " {" '(#\Newline) ,action '(#\Newline) "}"))

(defmacro r/ret (args)
  "Create Rust Return Struct"
  `(concatenate 'string "return " ,args";"))

(defmacro r/expr (args conditional)
  "Create Rust Expression"
  `(concatenate 'string ,args ,conditional))

;; This doesn't currently work, only returns NIL
(defmacro r/print (typ &optional args compargs)
  `(cond
     ((equal ,typ "!")
      (concatenate 'string
		   "print!(" (concatenate 'string (format NIL "\"") ,args (format NIL "\"")),compargs ");" '(#\Newline))
      ((equal ,typ "ln")
       `(concatenate 'string
		    "println!(" (concatenate 'string (format NIL "\"") ,args (format NIL "\"")),compargs ");" '(#\Newline)))
      ((equal ,typ "e")
       (concatenate 'string
		    "eprint!(" (concatenate 'string (format NIL "\"") ,args (format NIL "\"")),compargs ");" '(#\Newline)))
      ((equal ,typ "eln")
       (concatenate 'string
		    "eprintln!(" (concatenate 'string (format NIL "\"") ,args (format NIL "\"")),compargs ");" '(#\Newline))))))

(defmacro r/println! (args &optional compargs)
  "Macro that writes a Rust style printlin! function"
  `(concatenate 'string
		"println!(" (concatenate 'string (format NIL "\"") ,args (format NIL "\"")),compargs ");" '(#\Newline)))

(defmacro r/} (&optional pad)
  "Create Rust } close"
  `(if (equal ,pad t)
       (concatenate 'string "}" '(#\Newline))
       (concatenate 'string " }")))

(defmacro r/nl (pad args)
  "Create newline pad left, right, or both sides"
  `(cond
     ((equal ,pad "b")
      (concatenate 'string '(#\Newline) ,args '(#\Newline)))
     ((equal ,pad "l")
      (concatenate 'string '(#\Newline) ,args))
     ((equal ,pad "r")
      (concatenate 'string ,args '(#\Newline)))))

(defmacro l->r (&optional io file &rest body)
  "Lisp to Rust Struct, if io = t, outputs to main.rs in the current dir"
  `(if (equal ,io t)
       (let
	   ((in (open ,file :direction :output :if-exists :supersede)))
	 (format in "~a" (concatenate 'string
				       '(#\Newline) ,@body '(#\Newline)))
	 (close in))
       (concatenate 'string
		    '(#\Newline) ,@body '(#\Newline))))
