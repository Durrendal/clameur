# Clameur
```
Clameur: French "Clamor -- A tumultous cry"
```

## What Clameur is:

Clameur is a collections of lisp macros that aims to provide a domain specific language for the creation of Rust code, inside of Common lisp. It is a work in progress, and is provided for reference in hopes that it is useful for some someone.

## What Clameur is not:

Clameur is not an implementation of Rust inside of Lisp, it does not attempt to bind a Lisp world in chains as imposed by Rust's spec, rather it is a methodology for the creation of Rust programs.

## API:

### Rust Macros:
* (l->r) -- Lisp to Rust wrapper (allows creation of main.rs if passed t)
* (r/fn) -- Rust fn
* (r/cfn) -- Rust custom fn -- CURRENTLY BROKEN
* (r/match) -- Create a Rust Case
* (r/io) -- Create a Rust io:: struct
* (r/let) -- Rust lexical bind
* (r/let{) -- (r/let) but with { ,@body }
* (r/if) -- Rust if
* (r/elif -- Rust else if
* (r/else) -- Rust else
* (r/for) -- Rust for
* (r/ret) -- Rust return X struct
* (r/expr) -- Rust quantitative struct
* (r/print) -- Singular macro for all print functions (print, println eprint eptrinln) -- CURRENTLY BROKEN
* (r/println!) -- Rust println! (in for backward compatibility until r/print is functional
* (r/}) -- Adds a } + padding if passed t
* (r/nl) -- Adds newline padding, can be pased "l, r, b" to add a newline left, right, or on both sides

### Helper Macros:

* (w/input) -- Helper for (r/io) to create input &args
* (w/expect) -- Helper for (r/io) to create expect args
* (w/ercs) -- General error code helper
* (w/mat) -- Match helper to pass check vals and match vals to (r/match)


### Usage:

The example below shows how to use Clameur to reproduce the functional rewrite of FizzBuzz found [in the official Rust documentation] (https://doc.rust-lang.org/rust-by-example/fn.html)
An example .clm file is found in Examples, to test it yourself simply sbcl --load clameur.lisp then issue (load #P"Examples/fizzbuzz.clm") which will run and return T, after which the fizzbuzz rust code will be created in the directory sbcl was loaded in.

```
(l->r t
      (r/fn "main" "()"
            (r/cfn "fizzbuzz_to" "100"))
      (r/nl "l" (r/fn "is_divisible_by" "(lhs: u32, rhs: u32) -> bool"
                      (r/if "rhs == " "0"
                            (r/nl "r" (r/ret "false"))
                            (r/} t))
                      (r/expr "lhs % rhs == " "0")))
      (r/nl "l" (r/fn "fizzbuzz" "(n: u32) -> ()"
                      (r/if "is_divisible_by" "(n, 15)" (r/println! "fizzbuzz")
                            (r/elif "is_divisible_by" "(n, 3)" (r/println! "fizz"))
                            (r/elif "is_divisible_by" "(n, 5)" (r/println! "buzz"))
                            (r/else (r/println! "{}" ", n")))))
      (r/nl "l" (r/fn "fizzbuzz_to" "(n: u32)"
                      (r/for "n" "1..n +1" "fizzbuzz(n);"))))
```

It generates the below verbatim. Unfortunately Clameur does not at this time produce properly formatted Rust code.

```
fn main() { 
fizzbuzz_to(100);
}
fn is_divisible_by(lhs: u32, rhs: u32) -> bool { 
if rhs == 0 {
return false;
}
lhs % rhs == 0
}
fn fizzbuzz(n: u32) -> () { 
if is_divisible_by(n, 15) {
println!("fizzbuzz");
} else if is_divisible_by(n, 3) {
println!("fizz");
} else if is_divisible_by(n, 5) {
println!("buzz");
} else {
println!("{}", n);
}
}
fn fizzbuzz_to(n: u32) { 
for n in 1..n +1 {
fizzbuzz(n);
}
}
```