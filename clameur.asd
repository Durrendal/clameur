;;; Clameur.asd

;; This file is part of Clameur

;; Clameur is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; Clameur is distributed in the hope that it will be useful,
;; but WITHOUT WARRANTY; without even the implied warranty of
;; MERCHANTABILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with Clameur. If not, see <https://www.gnu.org/licenses/>.

(defpackage #:clameur
  (:use :cl :asdf))

(asdf:defsystem #:clameur
    :description "Lisp Rust code creation library"
    :author "Will Sinatra <wpsinatra@gmail.com>"
    :license "GPLv3"
    #+asdf-unicode :encoding #+asdf-unicode :utf-8
    :serial NIL
    :depends-on ()
    :components ((:file "clameur")))
