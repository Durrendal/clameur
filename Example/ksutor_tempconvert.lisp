(l->r t "clm_tempconvert.rs"
      (r/fn "main" "()"
	    (r/let "input" "String::new()")
	    (r/println! "What Temperature would you like to convert?")
	    (r/println! "1. Fahrenheit to Celsius")
	    (r/println! "2. Fahrenheit to Kelvin")
	    (r/println! "3. Celsius to Fahrenheit")
	    (r/println! "4. Celsius to Kelvin")
	    (r/println! "5. Kelvin to Fahrenheit")
	    (r/println! "6. Kelvin to Celsius")

	    (r/io "stdin" (w/input "read_line" "&mut input") (w/expect "Failed to Read Line"))
	    (r/let{ "input: u8" "match input.trim().parse()"
		    (w/ercs "num" "num" "_" "0"))
	    (r/println! "Plase enter a temperature")
	    (r/cfn "convert_temp" "input")
	    (r/cfn "convert_again" "()"))
      
      (r/cfn "convert_temp" "input: u8"
	     (r/let "mut temp" "String::new();")
	     (r/io "stdin" (w/input "read_line" "&mut temp") (w/expect "Failed to Read Line"))
	     (r/let{ "temp: f32" "temp.trim().parse()"
		     (w/ercs "num" "num" "_" "0.0")
		     (r/match "input"
			      (w/mat "1" "f_to_c(&temp)")
			      (w/mat "2" "f_to_k(&temp)")
			      (w/mat "3" "c_to_f(&temp)")
			      (w/mat "4" "c_to_k(&temp)")
			      (w/mat "5" "k_to_f(&temp)")
			      (w/mat "6" "k_to_c(&temp)")
			      (w/mat "_" (r/println! "Did not choose a valid option, wasting your time successful")))))
	     
	     (r/cfn "f_to_c" "temp: &f32"
		    (r/let "newtemp" "(temp - 32.0) * 5.0 / 9.0")
		    (println! "{} degrees Fahrenheit is {} degress Celcius" ", temp, newtemp"))

	     (r/cfn "f_to_k" "temp: &f32"
		    (r/let "newtemp" "((temp - 32.0) * 5.0 / 9.0) - 273.15")
		    (println! "{} degrees Celsius is {} degrees Kelvin" ", temp, newtemp"))

	     (r/cfn "c_to_f" "temp: &f32"
		    (r/let "newtemp" "(temp + 32.0) * 9.0 / 5.0")
		    (println! "{} degrees Celsius is {} degrees Fahrenheit" ", temp, newtemp"))

	     (r/cfn "c_to_k" "temp: &f32"
		    (r/let "newtemp" "temp + 273.15")
		    (println! "{} degrees Celsius is {} degrees Kelvin" ", temp, newtemp"))

	     (r/cfn "k_to_f" "temp: &f32"
		    (r/let "newtemp" "((temp - 273.15) * 9.0 / 5.0) + 32.0")
		    (println! "{} degrees Kelvin is {} degrees Fahrenheit" ", temp, newtemp"))

	     (r/cfn "k_to_c" "temp: &f32"
		    (r/let "newtemp" "temp -273.15")
		    (println! "{} degrees Kelvin is {} degrees Fahrenheit" ", temp, newtemp"))

	     (r/cfn "convert_again" "()"
		    (r/let "mut again" "String::new();")
		    (println! "\nWould you like to convert another temperature? (y/n)")
		    (r/io "stdin" (w/input "read_line" "&mut again") (w/expect "Failed to Read Line"))
		    (r/match "again.trim()"
			     (w/mat "y" "{ println!(""); main(); }")
			     (w/mat "n" (println! "Toodaloo"))
			     (w/mat "_" "convert_again()"))))
	    
